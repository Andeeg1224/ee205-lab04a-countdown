///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Andee Gary <andeeg@hawaii.edu>
// @date   04_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include<time.h>
#include <unistd.h>
int main(int argc, char* argv[]) {
   //Struct and time_t for current time
   time_t current;
   struct tm *currenttime;
   //Struct and time_t for reference time
   time_t referencetime;
   struct tm reference;
   //Integers for finding the years days, etc...
   int years;
   int days;
   int hours;
   int min;
   int sec;
   
      //Setting the reference date
      int refyear = 2001;
      reference.tm_year = refyear - 1900;
      reference.tm_mon = 11;
      reference.tm_mday = 24;
      reference.tm_hour = 2;
      reference.tm_min = 12;
      reference.tm_sec = 30;
      reference.tm_isdst = 0;

      //Converting everything into time_t then finding the difference in seconds
      referencetime = mktime (&reference);
      //double difference = difftime (current, referencetime);
      printf("Reference time and date:%s ", asctime(&reference));
      while(1){
      time(&current);
      currenttime = localtime(&current);
      double difference = difftime (current, referencetime);
      if(difference < 0){
         difference = difftime(referencetime, current);
           }
      else{
      //Checks if the reference year is a leap year and adjusts the difference to make correct output
      if (refyear % 400 != 0){
      if (refyear % 4 == 1) {
         difference -= 3600 * 6;
      }
      else if (refyear % 4 == 2) {
         difference -= 3600 * 2 * 6;
      } 
      else if (refyear % 4 == 3) {
         difference -= 3600 * 3 * 6;
      }
      } 
      }
      //Math behind calculations with 365.25 days a year to account for the leap year
      years = difference / (31557600);
      days = (difference - 31557600*years)/(86400);
      hours = (difference - 31557600*years - 86400*days)/(3600);
      min = (difference - 31557600*years - 86400*days - 3600*hours)/(60);
      sec= (difference - 31557600*years - 86400*days - 3600*hours - 60*min);
      printf("Years: %d Days: %d Hours: %d   Minutes: %d Seconds: %d\n", years, days, hours, min, sec);
      sleep(1);
      }
      

   return 0;
   
}
